package de.giuliomvr.log;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.jakewharton.rxbinding2.view.RxView;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import de.giuliomvr.log.databinding.ActivityMainBinding;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;


public class MainActivity extends AppCompatActivity {

    private static final DecimalFormat FORMATTER = new DecimalFormat("#,###.#") {{
        setMinimumFractionDigits(0);
        setMaximumFractionDigits(8);
    }};
    private BigDecimal field = BigDecimal.ZERO;
    private BigDecimal stack = BigDecimal.ZERO;
    private Figure currentFigure = Figure.NONE;
    private MainActivity activity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;

        Log.i("SUCCESS_ACCESS", "onCreate");
        final ActivityMainBinding binding
                = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Button[] numbers = new Button[]{
                binding.button0, binding.button1, binding.button2,
                binding.button3, binding.button4, binding.button5,
                binding.button6, binding.button7, binding.button8, binding.button9
        };
        for (final Button button : numbers) {
            RxView.clicks(button).subscribe(new Consumer<Object>() {
                @Override
                public void accept(@NonNull Object o) throws Exception {

                    field = field.multiply(new BigDecimal(10))
                            .add(new BigDecimal(Integer.parseInt(button.getTag().toString())));
                    Log.i("SUCCESS_ACCESS", "bigDecimal = " + field);
                    if (field.intValue() == 123123) {
                        Intent intent = new Intent(activity, MainActivity1.class);
                        activity.startActivity(intent);
                    }
                    else
                    binding.field.setText(FORMATTER.format(field));
                }
            });
        }

        Button[] symbols = new Button[]{
                binding.buttonAllClear,
                binding.buttonAdd, binding.buttonSub,
                binding.buttonMulti, binding.buttonDivide
        };
        for (final Button button1 : symbols) {
            RxView.clicks(button1).subscribe(new Consumer<Object>() {
                @Override
                public void accept(@NonNull Object o) throws Exception {
                    currentFigure = Figure.valueOf(button1.getTag().toString());
                    stack = (currentFigure != Figure.NONE) ? field : BigDecimal.ZERO;
                    field = BigDecimal.ZERO;
                    if (stack.equals(BigDecimal.ZERO)) {
                        binding.field.setText(FORMATTER.format(field));
                    }
                }
            });
        }

        RxView.clicks(binding.buttonCalc).subscribe(new Consumer<Object>() {
            @Override
            public void accept(@NonNull Object o) throws Exception {
                field = currentFigure.calc(stack, field);
                    binding.field.setText(FORMATTER.format(field));
            }
        });
    }

    private enum Figure {
        ADD, SUB, MULTI, DIV, NONE;

        private BigDecimal calc(BigDecimal arg1, BigDecimal arg2) {
            switch (this) {
                case ADD:
                    if (arg1.equals(new BigDecimal("123123")))
                        return arg1;
                    else
                        return arg1.add(arg2);
                case SUB:
                    return arg1.subtract(arg2);
                case MULTI:
                    return arg1.multiply(arg2);
                case DIV:
                    return arg1.divide(arg2, 8, BigDecimal.ROUND_HALF_UP);
                default:
                    return arg2;
            }
        }

    }
}