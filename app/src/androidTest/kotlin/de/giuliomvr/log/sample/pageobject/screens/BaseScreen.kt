package de.giuliomvr.log.sample.pageobject.screens

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.BySelector
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject2

abstract class BaseScreen(private val device: UiDevice) {

    val id = "${InstrumentationRegistry.getTargetContext().packageName}:id"

    fun find(by: BySelector): UiObject2 = device.findObject(by)

    fun click(by: BySelector) = find(by).click()
}
