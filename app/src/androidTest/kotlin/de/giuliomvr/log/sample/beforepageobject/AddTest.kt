package de.giuliomvr.log.sample.beforepageobject

import android.content.Context
import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AddTest {

    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = startDevice()
    }

    @Test
    fun addition() {
        device.findObject(By.res("io.github.alexilyenko.sample:id/button_2")).click()
        device.findObject(By.res("io.github.alexilyenko.sample:id/button_add")).click()
        device.findObject(By.res("io.github.alexilyenko.sample:id/button_8")).click()
        device.findObject(By.res("io.github.alexilyenko.sample:id/button_calc")).click()
        val result = device.findObject(By.res("io.github.alexilyenko.sample:id/field")).text.toInt()
        assertEquals("Result should be equal to 10", 10, result)
    }


    private fun startDevice(): UiDevice {
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        val pkg = InstrumentationRegistry.getTargetContext().packageName
        val context = InstrumentationRegistry.getContext()
        val intent = getIntent(context, pkg)
        val launchTimeout = 10000L

        context.startActivity(intent)
        device.wait(Until.hasObject(By.pkg(pkg).depth(0)), launchTimeout)
        return device
    }

    private fun getIntent(context: Context, pkg: String): Intent = context.packageManager
            .getLaunchIntentForPackage(pkg)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
}
